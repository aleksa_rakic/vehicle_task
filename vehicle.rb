# Vehicle Model
class Vehicle < ApplicationRecord
  validates :make, :model, :year, presence: true
  HOURLY_RATE = 5

  def number_to_currency(n)
    ## Need to check what is the responsibility of this method
  end

  def hourly_rate
    number_to_currency(HOURLY_RATE)
  end

  def travel(distance: 100)
    while distance > 0
      sleep 1
      puts "Only #{distance} miles to go!"
      distance -= 1
    end
  end

  def title
    "#{year} #{make} #{model}"
  end

  def start_ignition!
    puts "Ignition started!"
  end
end

class SmallVehicle < Vehicle
  def travel(distance: 100, message:)
    puts message
    super()
  end
  # more flexible solution:
  # def travel(distance: 100, message:)
  #   puts message
  #   super(distance: distance)
  # end
end

sv = SmallVehicle.new
sv.travel(distance: 300, message: 'message')